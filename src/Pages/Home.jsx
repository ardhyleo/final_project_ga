import Hero from "../Components/Hero/Hero";
import Trending from "../Components/Card/Trending";

function Home() {
  return (
    <>
      <Hero />
      <Trending />
    </>
  );
}

export default Home;
