import foto4 from "./assets/foto4.png";
import foto5 from "./assets/foto5.png";
import foto6 from "./assets/foto6.png";

const data2 = {
  campaign: [
    {
      id: 1,
      image: foto4,
      category: "Medical",
      title: "Aid for necessary items to help our country",
      author: "Aksi Cepat Tanggap",
      data_funding: 60,
      raised: "IDR 30.000.000",
      goal: "IDR 50.000.000",
    },

    {
      id: 2,
      image: foto5,
      category: "Medical",
      title: "Aid for necessary items to help our country",
      author: "Aksi Cepat Tanggap",
      data_funding: 60,
      raised: "IDR 30.000.000",
      goal: "IDR 50.000.000",
    },

    {
      id: 3,
      image: foto6,
      category: "Medical",
      title: "Aid for necessary items to help our country",
      author: "Aksi Cepat Tanggap",
      data_funding: 60,
      raised: "IDR 30.000.000",
      goal: "IDR 50.000.000",
    },
  ],
};

export default data2;
