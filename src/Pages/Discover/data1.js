import foto1 from "./assets/foto1.png";
import foto2 from "./assets/foto2.png";
import foto3 from "./assets/foto3.png";

const data1 = {
  campaign: [
    {
      id: 1,
      image: foto1,
      category: "Medical",
      title: "Aid for necessary items to help our country",
      author: "Aksi Cepat Tanggap",
      data_funding: 60,
      raised: "IDR 30.000.000",
      goal: "IDR 50.000.000",
    },

    {
      id: 2,
      image: foto2,
      category: "Medical",
      title: "Aid for necessary items to help our country",
      author: "Aksi Cepat Tanggap",
      data_funding: 60,
      raised: "IDR 30.000.000",
      goal: "IDR 50.000.000",
    },

    {
      id: 3,
      image: foto3,
      category: "Medical",
      title: "Aid for necessary items to help our country",
      author: "Aksi Cepat Tanggap",
      data_funding: 60,
      raised: "IDR 30.000.000",
      goal: "IDR 50.000.000",
    },
  ],
};

export default data1;
