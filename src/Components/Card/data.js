import Rectangle27 from "./assets/Rectangle 27.png";
import Rectangle26 from "./assets/Rectangle 26.png";
import Rectangle25 from "./assets/Rectangle25.png";
const data = {
  campaign: [
    {
      id: 1,
      image: Rectangle27,
      category: "Medical",
      title: "Aid for necessary items to help our country",
      author: "Aksi Cepat Tanggap",
      data_funding: 60,
      raised: "IDR 30.000.000",
      goal: "IDR 50.000.000",
    },

    {
      id: 2,
      image: Rectangle26,
      category: "Medical",
      title: "Aid for necessary items to help our country",
      author: "Aksi Cepat Tanggap",
      data_funding: 60,
      raised: "IDR 30.000.000",
      goal: "IDR 50.000.000",
    },

    {
      id: 3,
      image: Rectangle25,
      category: "Medical",
      title: "Aid for necessary items to help our country",
      author: "Aksi Cepat Tanggap",
      data_funding: 60,
      raised: "IDR 30.000.000",
      goal: "IDR 50.000.000",
    },
  ],
};

export default data;
