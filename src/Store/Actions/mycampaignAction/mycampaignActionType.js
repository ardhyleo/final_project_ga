const mycampaignActionType = {
    GET_CAMPAIGN_START: 'GET_CAMPAIGN_START',
    GET_CAMPAIGN_SUCCESS: 'GET_CAMPAIGN_SUCCESS',
    GET_CAMPAIGN_FAILED: 'GET_CAMPAIGN_FAILED'
}

export default mycampaignActionType