import {
  PROFILE_BEGIN,
} from "../../Constants/types";

export const ProfileAction = () => {
    return {
      type: PROFILE_BEGIN,
    };
  };